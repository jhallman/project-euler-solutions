# Problem 17: find the sum of all the letters in the written numbers from 1 to 1000
numbers = []

for i in range(1,1001):
	numbers.append(str(i))

ones_count = {'1':3,'2':3,'3':5,'4':4,'5':4,'6':3,'7':5,'8':5,'9':4,'0':0}
teens_count = {'10':3,'11':6,'12':6,'13':8,'14':8,'15':7,'16':7,'17':9,'18':8,'19':8}
tens_count = {'2':6,'3':6,'4':5,'5':5,'6':5,'7':7,'8':6,'9':6}

def count_ones(i, s):
	s += ones_count[i]
	return s

def count_tens(i, s):
	if i[0] == '0':
		s += ones_count[i[1]]
		#print str(s) + 'd'
	elif i[0] == '1':
		s += teens_count[i]
		#print str(s) + 'e'
	else:
		s += tens_count[i[0]]
		#print str(s) + 'f'
		if i[1] != '0':
			s += ones_count[i[1]]
			#print str(s) + 'g'
	return s

def count_hundreds(i, s):
	s += ones_count[i[0]] + 7
	#print str(s) + 'a'
	if i[1] != '0' or i[2] != '0':
		s += 3
		#print str(s) + 'b'
		s = count_tens(i[1:], s)
		#print str(s) + 'c'
	return s

ans = 0
for i in numbers:
	s = 0
	if len(i) == 1:
		s += count_ones(i,s)
	if len(i) == 2:
		s += count_tens(i,s)
	if len(i) == 3:
		s += count_hundreds(i,s)
	ans += s

print ans + 11





