# Problem 16: find the sum of the digits of the number 2^1000
num = 2
for i in range(999):
	num *= 2

lst = list(str(num))

intlst = []
for i in lst:
	intlst.append(int(i))

s = 0
for q in intlst:
	s += q

f = open('number.txt', 'w')
n = str(s)
f.write(n)
f.close