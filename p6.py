# find the difference between the sum of squares and the square 
# of sums of the first n numbers 

def asum(x):
  if x < 2:
    return x
  else: return x + asum(x-1)

def sq(x):
  return x*x

def sum_of_squares(x):
  s = 0
  while x > 0:
    s += sq(x)
    x -= 1
  return s

def square_of_sums(x):
  return sq(((x*x)+x)/2)