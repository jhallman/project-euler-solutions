# Problem 8: find the largest product of five consecutive digits 
#in the given 1000 digit number
from sys import argv
import string

script, filename = argv

txt = open(filename)
t = txt.read()
t = t.translate(None, string.whitespace)

lst = list(t)

num = []
for i in lst:
	num.append(int(i))

index = 0
maximum = None
while index < 995:
	prod = 1
	for i in range(5):
		prod *= num[index+i]
	if prod > maximum: maximum = prod
	index += 1

print maximum
